const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const typesofworksShema = new Schema({
  typeName: String
});

module.exports = mongoose.model(
  "typesofworks",
  typesofworksShema,
  "typesofworks"
);
