const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const suppliersSchema = new Schema({
  supplierName: String
});

module.exports = mongoose.model("suppliers", suppliersSchema, "suppliers");
