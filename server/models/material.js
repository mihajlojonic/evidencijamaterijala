const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const materialSchema = new Schema({
  location: { type: Schema.Types.ObjectId, ref: "location" },
  date: Date,
  supplier: { type: Schema.Types.ObjectId, ref: "suppliers" },
  typeofmaterial: String,
  quantity: Number,
  unitofmeasure: String,
  typeofwork: { type: Schema.Types.ObjectId, ref: "typesofworks" },
  amountPDV: Number,
  pricewithPDV: Number
});

module.exports = mongoose.model("material", materialSchema, "material");
