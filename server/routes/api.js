const express = require("express");
const config = require("config");
const jwt = require("jsonwebtoken");
const router = express.Router();
const User = require("../models/user");
const Material = require("../models/material");
const Location = require("../models/location");
const TypesofWorks = require("../models/typesofworks");
const Suppliers = require("../models/suppliers");
const monguse = require("mongoose");

monguse.connect(
  config.get("Customer.dbConfig").dbAddress,
  { useNewUrlParser: true },
  err => {
    if (err) {
      console.log("Error connect to database " + err);
    } else {
      console.log("Connected to mongodb");
    }
  }
);

function verifyToken(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send("Unauthorized request");
  }
  let token = req.headers.authorization.split(" ")[1];
  if (token === "null") {
    return res.status(401).send("Unauthorized request");
  }

  let payload = jwt.verify(token, "secretKey");

  if (!payload) {
    return res.status(401).send("Unauthorized request");
  }
  req.userId = payload.subject;
  next();
}

router.post("/register", verifyToken, (req, res) => {
  let userData = req.body;
  let user = new User(userData);
  user.save((err, registerUser) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(registerUser);
    }
  });
});

router.post("/login", (req, res) => {
  let userData = req.body;
  User.findOne({ email: userData.email }, (err, user) => {
    if (err) {
      console.log(err);
    } else {
      if (!user) {
        res.status(401).send("Invalid email");
      } else if (user.password !== userData.password) {
        res.status(401).send("Invalid password");
      } else {
        let payload = { subject: user._id };
        let token = jwt.sign(payload, "secretKey");
        res.status(200).send({ token });
      }
    }
  });
});

router.get("/material", verifyToken, (req, res) => {
  let returnObject = {};
  let returnStatus = 200;
  let startDate = req.query.startDate;
  let endDate = req.query.endDate;
  let location = req.query.location;

  if (
    startDate === undefined ||
    endDate === undefined ||
    location === undefined
  ) {
    returnStatus = 404;
    res.status(returnStatus).send({});
  } else {
    let startDateObj = new Date(startDate);
    let endDateObj = new Date(endDate);

    Material.find({
      date: { $lt: endDateObj, $gte: startDateObj },
      location: location
    })
      .populate(["location", "typeofwork", "supplier"])
      .exec(function(err, material) {
        if (err) {
          console.log(err);
        } else {
          res.status(200).send(material);
        }
      });
  }
});

router.get("/location", verifyToken, (req, res) => {
  Location.find().exec(function(err, locations) {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(locations);
    }
  });
});

router.get("/supplier", verifyToken, (req, res) => {
  Suppliers.find().exec(function(err, suppliers) {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(suppliers);
    }
  });
});

router.get("/typeofwork", verifyToken, (req, res) => {
  TypesofWorks.find().exec((err, types) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(types);
    }
  });
});

router.get("/material/:id", verifyToken, (req, res) => {
  let materialId = req.params.id;
  Material.findById(materialId)
    .populate(["location", "typeofwork", "supplier"])
    .exec((err, material) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send(material);
      }
    });
});

router.get("/location/:id", verifyToken, (req, res) => {
  let locationId = req.params.id;
  Location.findById(locationId).exec((err, location) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(location);
    }
  });
});

router.get("/supplier/:id", verifyToken, (req, res) => {
  let supplierId = req.params.id;
  Suppliers.findById(supplierId).exec((err, supplier) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(supplier);
    }
  });
});

router.get("/typeofwork/:id", verifyToken, (req, res) => {
  let typeofworkId = req.params.id;
  TypesofWorks.findById(typeofworkId).exec((err, typeofwork) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(typeofwork);
    }
  });
});

router.put("/material/:id", verifyToken, (req, res) => {
  let materialId = req.params.id;
  let materialNewData = req.body;
  Material.findByIdAndUpdate(
    materialId,
    materialNewData,
    { upsert: true, useFindAndModify: false },
    (err, response) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send({ message: "Updated!" });
      }
    }
  );
});

router.put("/location/:id", verifyToken, (req, res) => {
  let locationId = req.params.id;
  let locationNewData = req.body;
  Location.findByIdAndUpdate(
    locationId,
    locationNewData,
    { upsert: true, useFindAndModify: false },
    (err, response) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send({ message: "Updated!" });
      }
    }
  );
});

router.put("/supplier/:id", verifyToken, (req, res) => {
  let supplierId = req.params.id;
  let supplierNewData = req.body;
  Suppliers.findByIdAndUpdate(
    supplierId,
    supplierNewData,
    { upsert: true, useFindAndModify: false },
    (err, response) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send({ message: "Updated!" });
      }
    }
  );
});

router.put("/typeofwork/:id", verifyToken, (req, res) => {
  let typeofworkId = req.params.id;
  let typeofworkNewData = req.body;
  TypesofWorks.findByIdAndUpdate(
    typeofworkId,
    typeofworkNewData,
    { upsert: true, useFindAndModify: false },
    (err, response) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).send({ message: "Updated!" });
      }
    }
  );
});

router.post("/material", verifyToken, (req, res) => {
  let materialData = req.body;
  let material = new Material(materialData);
  material.save((err, addedMaterial) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(addedMaterial);
    }
  });
});

router.post("/location", verifyToken, (req, res) => {
  let locationData = req.body;
  let location = new Location(locationData);
  location.save((err, addedLocation) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(addedLocation);
    }
  });
});

router.post("/supplier", verifyToken, (req, res) => {
  let supplierData = req.body;
  let supplier = new Suppliers(supplierData);
  supplier.save((err, addedSupplier) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(addedSupplier);
    }
  });
});

router.post("/typeofwork", verifyToken, (req, res) => {
  let typeofworkData = req.body;
  let typeofwork = new TypesofWorks(typeofworkData);
  typeofwork.save((err, addedTypeofwork) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).send(addedTypeofwork);
    }
  });
});

router.delete("/material/:id", verifyToken, (req, res) => {
  let materialIdForDelete = req.params.id;
  console.log(materialIdForDelete);
  Material.findByIdAndDelete(materialIdForDelete, (err, response) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: "Deleted!" });
    }
  });
});

router.delete("/location/:id", verifyToken, (req, res) => {
  let locationIdForDelete = req.params.id;
  Location.findByIdAndDelete(locationIdForDelete, (err, response) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: "Deleted! " });
    }
  });
});

router.delete("/supplier/:id", verifyToken, (req, res) => {
  let supplierIdForDelete = req.params.id;
  Suppliers.findByIdAndDelete(supplierIdForDelete, (err, response) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: "Deleted!" });
    }
  });
});

router.delete("/typeofwork/:id", verifyToken, (req, res) => {
  let typeofworkIdForDelete = req.params.id;
  TypesofWorks.findByIdAndDelete(typeofworkIdForDelete, (err, response) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: "Deleted!" });
    }
  });
});

//REPORT
router.get("/report", verifyToken, (req, res) => {
  let returnObject = {};
  let returnStatus = 200;
  let startDate = req.query.startDate;
  let endDate = req.query.endDate;
  let location = req.query.location;

  if (
    startDate === undefined ||
    endDate === undefined ||
    location === undefined
  ) {
    returnObject = { message: "Wrong parameters!" };
    returnStatus = 404;
    res.status(returnStatus).send(returnObject);
  } else {
    let startDateObj = new Date(startDate);
    let endDateObj = new Date(endDate);

    Material.find({
      date: { $lt: endDateObj, $gte: startDateObj },
      location: location
    })
      .populate(["location", "typeofwork", "supplier"])
      .exec(function(err, material) {
        if (err) {
          console.log(err);
        } else {
          const sumPDV = material.reduce(
            (a, b) => a + (b["amountPDV"] || 0),
            0
          );
          const sumPriceWithPDV = material.reduce(
            (a, b) => a + (b["pricewithPDV"] || 0),
            0
          );
          returnObject.fullReport = {
            materials: material,
            sumPDV: sumPDV,
            sumPriceWithPDV: sumPriceWithPDV
          };
          returnStatus = 200;

          //grouping
          const mapMaterial = new Map();
          for (let i = 0; i < material.length; i++) {
            const key = material[i].supplier;
            const value = mapMaterial.get(key);
            if (!value) {
              mapMaterial.set(key, [material[i]]);
            } else {
              value.push(material[i]);
            }
          }

          let groupedBySuppliers = [];
          for (let [k, v] of mapMaterial) {
            const sumPDV = v.reduce((a, b) => a + (b["amountPDV"] || 0), 0);
            const sumPriceWithPDV = v.reduce(
              (a, b) => a + (b["pricewithPDV"] || 0),
              0
            );
            groupedBySuppliers.push({
              supplier: k,
              materials: v,
              sumPDV: sumPDV,
              sumPriceWithPDV: sumPriceWithPDV
            });
          }

          returnObject.groupedBySuppliers = groupedBySuppliers;

          res.status(returnStatus).send(returnObject);
        }
      });
  }
});

module.exports = router;
