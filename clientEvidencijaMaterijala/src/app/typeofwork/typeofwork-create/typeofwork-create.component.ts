import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RepositoryService } from "src/app/shared/repository.service";
import { MatDialog } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { TypesOfWorksModelForCreate } from "src/app/interface/ForCreate/TypeOfWorkModelForCreate";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-typeofwork-create",
  templateUrl: "./typeofwork-create.component.html",
  styleUrls: ["./typeofwork-create.component.css"]
})
export class TypeofworkCreateComponent implements OnInit {
  public typesofworksForm: FormGroup;
  private dialogConfig;

  constructor(
    private location: Location,
    private repository: RepositoryService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.typesofworksForm = new FormGroup({
      typeName: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.typesofworksForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public createTypeofwork = typeofworkFormValue => {
    if (this.typesofworksForm.valid) {
      this.executeTypeofworkCreation(typeofworkFormValue);
    }
  };

  private executeTypeofworkCreation = typeofworkFormValue => {
    let typeofworkForCreate: TypesOfWorksModelForCreate = {
      typeName: typeofworkFormValue.typeName
    };

    this.repository.create("api/typeofwork", typeofworkForCreate).subscribe(
      res => {
        let dialogRef = this.dialog.open(
          SuccessDialogComponent,
          this.dialogConfig
        );

        dialogRef.afterClosed().subscribe(result => {
          this.location.back();
        });
      },
      err => {
        this.location.back();
      }
    );
  };
}
