import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { TypesOfWorksModel } from "src/app/interface/TypesOfWorksModel";
import { Location } from "@angular/common";
import { RepositoryService } from "src/app/shared/repository.service";
import { MatDialog } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { TypesOfWorksModelForCreate } from "src/app/interface/ForCreate/TypeOfWorkModelForCreate";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-typeofwork-update",
  templateUrl: "./typeofwork-update.component.html",
  styleUrls: ["./typeofwork-update.component.css"]
})
export class TypeofworkUpdateComponent implements OnInit {
  public typeofworksForm: FormGroup;
  private dialogConfig;

  public typeofworksModel: TypesOfWorksModel = null;

  constructor(
    private location: Location,
    private repository: RepositoryService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.typeofworksForm = new FormGroup({
      typeName: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };

    this.setTypeofworkForUpdate();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.typeofworksForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public setTypeofworkForUpdate = () => {
    let id: string = this.activeRoute.snapshot.params["id"];
    let apiUrl: string = `api/typeofwork/${id}`;
    let supplierTmp: TypesOfWorksModel;
    this.repository.getData(apiUrl).subscribe(
      res => {
        this.typeofworksModel = res as TypesOfWorksModel;

        this.typeofworksForm.controls["typeName"].setValue(
          this.typeofworksModel.typeName
        );
      },
      error => {
        console.log(error);
      }
    );
  };

  public updateTypeofworks = typeofworksFormValue => {
    if (this.typeofworksForm.valid) {
      this.executeTypeofworkUpdate(typeofworksFormValue);
    }
  };

  private executeTypeofworkUpdate = typeofworksFormValue => {
    let typeofworksForUpdate: TypesOfWorksModelForCreate = {
      typeName: typeofworksFormValue.typeName
    };

    this.repository
      .update(
        "api/typeofwork/" + this.typeofworksModel._id,
        typeofworksForUpdate
      )
      .subscribe(
        res => {
          let dialogRef = this.dialog.open(
            SuccessDialogComponent,
            this.dialogConfig
          );

          dialogRef.afterClosed().subscribe(result => {
            this.location.back();
          });
        },
        err => {
          this.location.back();
        }
      );
  };
}
