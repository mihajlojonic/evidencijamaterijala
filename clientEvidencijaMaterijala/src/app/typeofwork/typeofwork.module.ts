import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TypeofworkListComponent } from "./typeofwork-list/typeofwork-list.component";
import { TypeofworkCreateComponent } from "./typeofwork-create/typeofwork-create.component";
import { TypeofworkUpdateComponent } from "./typeofwork-update/typeofwork-update.component";
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { TypeofworkRoutingModule } from "./typeofwork-routing/typeofwork-routing.module";

@NgModule({
  declarations: [
    TypeofworkListComponent,
    TypeofworkCreateComponent,
    TypeofworkUpdateComponent
  ],
  imports: [
    CommonModule,
    TypeofworkRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class TypeofworkModule {}
