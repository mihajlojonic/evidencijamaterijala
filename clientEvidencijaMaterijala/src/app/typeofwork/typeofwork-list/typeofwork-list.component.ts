import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { TypesOfWorksModel } from "src/app/interface/TypesOfWorksModel";
import { MatTableDataSource, MatDialog, MatPaginator } from "@angular/material";
import { RepositoryService } from "src/app/shared/repository.service";
import { Router } from "@angular/router";
import { DeleteConfirmComponent } from "src/app/shared/dialogs/delete-confirm/delete-confirm.component";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-typeofwork-list",
  templateUrl: "./typeofwork-list.component.html",
  styleUrls: ["./typeofwork-list.component.css"]
})
export class TypeofworkListComponent implements OnInit, AfterViewInit {
  public displayedColumns = ["VrstaRadova", "Izmeni", "Obrisi"];

  public dataSource = new MatTableDataSource<TypesOfWorksModel>();
  private deleteConfirmDialogConfig;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private repoService: RepositoryService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getAllTypesOfWorks();

    this.deleteConfirmDialogConfig = {
      height: "250px",
      width: "400px",
      disableClose: true,
      data: {}
    };
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  public getAllTypesOfWorks = () => {
    this.repoService.getData("api/typeofwork").subscribe(res => {
      this.dataSource.data = res as TypesOfWorksModel[];
    });
  };

  public redirectToUpdate = (id: string) => {
    let url: string = `/typeofworks/update/${id}`;
    this.router.navigate([url]);
  };

  public redirectToDelete = (id: string) => {
    this.deleteConfirmDialogConfig.data = {
      type: "Typeofwork",
      valueReturn: false
    };
    let dialogRef = this.dialog.open(
      DeleteConfirmComponent,
      this.deleteConfirmDialogConfig
    );

    dialogRef.afterClosed().subscribe(result => {
      if (this.deleteConfirmDialogConfig.data.valueReturn == true)
        this.doDelete(id);
    });
  };

  public doDelete = (id: string) => {
    let url: string = `api/typeofwork/${id}`;
    this.repoService.delete(url).subscribe(
      res => {
        this.deleteConfirmDialogConfig.data = {};
        let dialogSuccesRef = this.dialog.open(
          SuccessDialogComponent,
          this.deleteConfirmDialogConfig
        );

        dialogSuccesRef.afterClosed().subscribe(result => {
          this.getAllTypesOfWorks();
        });
      },
      error => {
        console.log(error);
      }
    );
  };
}
