import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TypeofworkListComponent } from "../typeofwork-list/typeofwork-list.component";
import { TypeofworkCreateComponent } from "../typeofwork-create/typeofwork-create.component";
import { TypeofworkUpdateComponent } from "../typeofwork-update/typeofwork-update.component";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "typeofworks", component: TypeofworkListComponent },
  { path: "create", component: TypeofworkCreateComponent },
  { path: "update/:id", component: TypeofworkUpdateComponent }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypeofworkRoutingModule {}
