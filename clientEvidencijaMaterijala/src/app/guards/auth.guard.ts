import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { RepositoryService } from "../shared/repository.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private _repo: RepositoryService, private _router: Router) {}

  canActivate(): boolean {
    if (this._repo.loggedIn()) {
      return true;
    } else {
      this._router.navigate(["/login"]);
      return false;
    }
  }
}
