import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { UserModel } from "../interface/UserModel";
import { RepositoryService } from "../shared/repository.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  public user: UserModel = null;
  constructor(
    private location: Location,
    private repository: RepositoryService,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.loginForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public doLogin = loginFormValue => {
    if (this.loginForm.valid) {
      this.executeLogin(loginFormValue);
    }
  };

  private executeLogin = loginFormValue => {
    let userForLoging: UserModel = {
      email: loginFormValue.email,
      password: loginFormValue.password
    };

    this.repository.loginUser(userForLoging).subscribe(
      res => {
        console.log(res);
        localStorage.setItem("token", res["token"]);
        this.router.navigate(["/materials/materials"]);
      },
      err => console.log(err)
    );
  };
}
