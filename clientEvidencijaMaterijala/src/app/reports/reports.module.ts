import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReportGeneratorComponent } from "./report-generator/report-generator.component";
import { MaterialsRoutingModule } from "../materials/materials-routing/materials-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule, MatNativeDateModule } from "@angular/material";
import { SharedModule } from "../shared/shared.module";
import { ReportsRoutingModule } from "./reports-routing/reports-routing.module";

@NgModule({
  declarations: [ReportGeneratorComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule
  ]
})
export class ReportsModule {}
