import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReportGeneratorComponent } from "../report-generator/report-generator.component";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "reportgenerator", component: ReportGeneratorComponent }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule {}
