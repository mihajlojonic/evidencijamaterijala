import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LocationModel } from "src/app/interface/LocationModel";
import { Location } from "@angular/common";
import { RepositoryService } from "src/app/shared/repository.service";
import { ReportModel } from "src/app/interface/Report/ReportModel";
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from "pdfmake/build/vfs_fonts";
import { FullReportModel } from "src/app/interface/Report/FullReportModel";
import { GroupedReportItem } from "src/app/interface/Report/GroupedReportItem";
import { MatDialog } from "@angular/material";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";
import { ErrorDialogComponent } from "src/app/shared/dialogs/error-dialog/error-dialog.component";

@Component({
  selector: "app-report-generator",
  templateUrl: "./report-generator.component.html",
  styleUrls: ["./report-generator.component.css"]
})
export class ReportGeneratorComponent implements OnInit {
  public reportForm: FormGroup;
  public locations: LocationModel[] = [];

  public reportData: ReportModel;

  public month: string = "";
  public year: string = "";

  public monthSerbian: string[] = [
    "Jan",
    "Feb",
    "Mart",
    "April",
    "Maj",
    "Jun",
    "Jul",
    "Avg",
    "Sep",
    "Okt",
    "Dec"
  ];

  private dialogConfig;

  constructor(
    public repository: RepositoryService,
    public location: Location,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.reportForm = new FormGroup({
      location: new FormControl("", [Validators.required]),
      dateFrom: new FormControl("", [Validators.required]),
      dateTo: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };

    this.getLocations();
  }

  getLocations() {
    this.repository.getData("api/location").subscribe(res => {
      this.locations = res as LocationModel[];
    });
  }

  public onCancel = () => {
    this.location.back();
  };

  public generateReport = reportFormValue => {
    this.month = new Date(reportFormValue.dateFrom).getMonth().toString();
    this.year = new Date(reportFormValue.dateFrom).getFullYear().toString();

    if (reportFormValue.dateFrom > reportFormValue.dateTo) {
      let dialogRef = this.dialog.open(ErrorDialogComponent, this.dialogConfig);

      return;
    }

    this.repository
      .getData(
        "api/report?startDate=" +
          reportFormValue.dateFrom.toISOString() +
          "&endDate=" +
          reportFormValue.dateTo.toISOString() +
          "&location=" +
          reportFormValue.location
      )
      .subscribe(res => {
        this.reportData = res as ReportModel;

        this.buildPdf();
      });
  };

  buildPdf() {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    let numOfPageBreaks = 0;

    if (this.reportData.fullReport.materials.length == 0) {
      let dialogRef = this.dialog.open(ErrorDialogComponent, this.dialogConfig);
      return;
    }

    this.template.content[0].table.body[0][0].text =
      "EVIDENCIJA PRIMLJENOG MATERIJALA - LOKACIJA:" +
      this.reportData.fullReport.materials[0].location.name +
      " - " +
      this.monthSerbian[this.month] +
      " " +
      this.year;

    //full report first
    for (let i = 0; i < this.reportData.fullReport.materials.length; i++) {
      let dateObj = new Date(this.reportData.fullReport.materials[i].date);
      let dateString =
        dateObj.getDate() +
        "-" +
        this.monthSerbian[dateObj.getMonth()] +
        "-" +
        dateObj.getFullYear();

      this.template.content[0].table.body.push([
        {
          text: dateString,
          fontSize: 10
        },
        {
          text: this.reportData.fullReport.materials[i].supplier.supplierName,
          fontSize: 10
        },
        {
          text: this.reportData.fullReport.materials[i].typeofmaterial,
          fontSize: 10
        },
        {
          text: this.reportData.fullReport.materials[i].quantity,
          fontSize: 10
        },
        {
          text: this.reportData.fullReport.materials[i].unitofmeasure,
          fontSize: 10
        },
        {
          text: this.reportData.fullReport.materials[i].typeofwork.typeName,
          fontSize: 10
        },
        {
          text:
            this.reportData.fullReport.materials[i].amountPDV == 0
              ? ""
              : this.reportData.fullReport.materials[i].amountPDV,
          fontSize: 10
        },
        {
          text:
            this.reportData.fullReport.materials[i].pricewithPDV == 0
              ? ""
              : this.reportData.fullReport.materials[i].pricewithPDV,
          fontSize: 10
        }
      ]);
    }

    this.template.content[0].table.body.push([
      { text: "Ukupno:", fontSize: 10 },
      { text: "", fontSize: 10 },
      { text: "", fontSize: 10 },
      { text: "", fontSize: 10 },
      { text: "", fontSize: 10 },
      { text: "", fontSize: 10 },
      { text: this.reportData.fullReport.sumPDV, fontSize: 10 },
      {
        text: this.reportData.fullReport.sumPriceWithPDV,
        fontSize: 10
      }
    ]);

    this.template.content.push({ text: "", pageBreak: "after" });
    numOfPageBreaks++;

    //grouped tables

    for (let i = 0; i < this.reportData.groupedBySuppliers.length; i++) {
      this.template.content.push({
        table: {
          headerRows: 2,
          pageBreak: "after",
          widths: ["auto", "auto", "auto", "auto", "auto", "*", "auto", "auto"],
          body: [
            [
              {
                text:
                  this.reportData.groupedBySuppliers[i].supplier.supplierName +
                  ":EVIDENCIJA PRIMLJENOG MATERIJALA - LOKACIJA:" +
                  this.reportData.groupedBySuppliers[i].materials[0].location
                    .name +
                  " - " +
                  this.monthSerbian[this.month] +
                  " " +
                  this.year,
                colSpan: 8,
                alignment: "center",
                bold: true
              },
              {},
              {},
              {},
              {},
              {},
              {},
              {}
            ],
            [
              { text: "Datum", bold: true },
              { text: "Dobavljac", bold: true },
              { text: "Vrsta materijala", bold: true },
              { text: "Kolicina", bold: true },
              { text: "Jedinica mere", bold: true },
              { text: "Vrste radova", bold: true },
              { text: "Iznos PDV-a", bold: true },
              { text: "Vrednost sa PDV-om", bold: true }
            ]
          ]
        }
      });

      for (
        let j = 0;
        j < this.reportData.groupedBySuppliers[i].materials.length;
        j++
      ) {
        let dateObj = new Date(
          this.reportData.groupedBySuppliers[i].materials[j].date
        );
        let dateString =
          dateObj.getDate() +
          "-" +
          this.monthSerbian[dateObj.getMonth()] +
          "-" +
          dateObj.getFullYear();

        this.template.content[i + 1 + numOfPageBreaks].table.body.push([
          {
            text: dateString,
            fontSize: 10
          },
          {
            text: this.reportData.groupedBySuppliers[i].materials[j].supplier
              .supplierName,
            fontSize: 10
          },
          {
            text: this.reportData.groupedBySuppliers[i].materials[j]
              .typeofmaterial,
            fontSize: 10
          },
          {
            text: this.reportData.groupedBySuppliers[i].materials[j].quantity,
            fontSize: 10
          },
          {
            text: this.reportData.groupedBySuppliers[i].materials[j]
              .unitofmeasure,
            fontSize: 10
          },
          {
            text: this.reportData.groupedBySuppliers[i].materials[j].typeofwork
              .typeName,
            fontSize: 10
          },
          {
            text:
              this.reportData.groupedBySuppliers[i].materials[j].amountPDV == 0
                ? ""
                : this.reportData.groupedBySuppliers[i].materials[j].amountPDV,
            fontSize: 10
          },
          {
            text:
              this.reportData.groupedBySuppliers[i].materials[j].pricewithPDV ==
              0
                ? ""
                : this.reportData.groupedBySuppliers[i].materials[j]
                    .pricewithPDV,
            fontSize: 10
          }
        ]);
      }

      this.template.content[i + 1 + numOfPageBreaks].table.body.push([
        { text: "Ukupno:", fontSize: 10 },
        { text: "", fontSize: 10 },
        { text: "", fontSize: 10 },
        { text: "", fontSize: 10 },
        { text: "", fontSize: 10 },
        { text: "", fontSize: 10 },
        { text: this.reportData.groupedBySuppliers[i].sumPDV, fontSize: 10 },
        {
          text: this.reportData.groupedBySuppliers[i].sumPriceWithPDV,
          fontSize: 10
        }
      ]);

      if (i + 1 != this.reportData.groupedBySuppliers.length) {
        this.template.content.push({ text: "", pageBreak: "after" });
        numOfPageBreaks++;
      }
    }
    pdfMake.createPdf(this.template).open();
  }

  template: any = {
    pageSize: "A4",
    pageOrientation: "landscape",
    content: [
      {
        table: {
          headerRows: 2,
          pageBreak: "after",
          widths: ["auto", "auto", "auto", "auto", "auto", "*", "auto", "auto"],
          body: [
            [
              {
                text: "Header with Colspan = 8",
                colSpan: 8,
                alignment: "center",
                bold: true
              },
              {},
              {},
              {},
              {},
              {},
              {},
              {}
            ],
            [
              { text: "Datum", bold: true },
              { text: "Dobavljac", bold: true },
              { text: "Vrsta materijala", bold: true },
              { text: "Kolicina", bold: true },
              { text: "Jedinica mere", bold: true },
              { text: "Vrste radova", bold: true },
              { text: "Iznos PDV-a", bold: true },
              { text: "Vrednost sa PDV-om", bold: true }
            ]
          ]
        }
      }
    ]
  };
}
