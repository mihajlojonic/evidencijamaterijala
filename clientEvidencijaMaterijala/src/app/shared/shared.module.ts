import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SuccessDialogComponent } from "./dialogs/success-dialog/success-dialog.component";
import { MaterialModule } from "../material/material.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { DeleteConfirmComponent } from "./dialogs/delete-confirm/delete-confirm.component";
import { ErrorDialogComponent } from "./dialogs/error-dialog/error-dialog.component";

@NgModule({
  declarations: [
    SuccessDialogComponent,
    DeleteConfirmComponent,
    DeleteConfirmComponent,
    ErrorDialogComponent
  ],
  imports: [CommonModule, MaterialModule, FlexLayoutModule],
  exports: [
    SuccessDialogComponent,
    MaterialModule,
    FlexLayoutModule,
    DeleteConfirmComponent,
    ErrorDialogComponent
  ],
  entryComponents: [
    SuccessDialogComponent,
    DeleteConfirmComponent,
    ErrorDialogComponent
  ]
})
export class SharedModule {}
