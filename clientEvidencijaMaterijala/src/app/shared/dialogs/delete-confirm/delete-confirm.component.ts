import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

@Component({
  selector: "app-delete-confirm",
  templateUrl: "./delete-confirm.component.html",
  styleUrls: ["./delete-confirm.component.css"]
})
export class DeleteConfirmComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DeleteConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  public closeDialog = () => {
    this.data.valueReturn = false;
    this.dialogRef.close();
  };

  public confirmDeleteDialog = () => {
    this.data.valueReturn = true;
    this.dialogRef.close();
  };
}
