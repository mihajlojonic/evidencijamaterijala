import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
  { path: "home", component: HomeComponent, canActivate: [AuthGuard] },
  {
    path: "materials",
    loadChildren: "./materials/materials.module#MaterialsModule",
    canActivate: [AuthGuard]
  },
  {
    path: "reports",
    loadChildren: "./reports/reports.module#ReportsModule",
    canActivate: [AuthGuard]
  },
  {
    path: "locations",
    loadChildren: "./location/location.module#LocationModule",
    canActivate: [AuthGuard]
  },
  {
    path: "suppliers",
    loadChildren: "./supplier/supplier.module#SupplierModule",
    canActivate: [AuthGuard]
  },
  {
    path: "typeofworks",
    loadChildren: "./typeofwork/typeofwork.module#TypeofworkModule",
    canActivate: [AuthGuard]
  },
  { path: "login", component: LoginComponent },
  { path: "", redirectTo: "/home", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
