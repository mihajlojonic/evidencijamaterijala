import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RepositoryService } from "src/app/shared/repository.service";
import { Location } from "@angular/common";
import { LocationModel } from "src/app/interface/LocationModel";
import { SupplierModel } from "src/app/interface/SupplierModel";
import { TypesOfWorksModel } from "src/app/interface/TypesOfWorksModel";
import { MaterialModelForCreate } from "src/app/interface/ForCreate/MaterialModelForCreate";
import { MatDialog } from "@angular/material";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";
import { ActivatedRoute } from "@angular/router";
import { MaterialModel } from "src/app/interface/MaterialModel";

@Component({
  selector: "app-materials-create",
  templateUrl: "./materials-create.component.html",
  styleUrls: ["./materials-create.component.css"]
})
export class MaterialsCreateComponent implements OnInit {
  public materialsForm: FormGroup;
  public locations: LocationModel[] = [];
  public suppliers: SupplierModel[] = [];
  public typesofworks: TypesOfWorksModel[] = [];
  private dialogConfig;

  constructor(
    private location: Location,
    private repository: RepositoryService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.materialsForm = new FormGroup({
      location: new FormControl("", [Validators.required]),
      typeofmaterial: new FormControl("", [
        Validators.required,
        Validators.maxLength(60)
      ]),
      date: new FormControl(new Date()),
      quantity: new FormControl("", [Validators.required]),
      unitofmeasure: new FormControl("", [
        Validators.required,
        Validators.maxLength(100)
      ]),
      typeofwork: new FormControl("", [Validators.required]),
      amountPDV: new FormControl(),
      pricewithPDV: new FormControl(),
      supplier: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };

    this.getLocations();
    this.getSuppliers();
    this.getTypesOfWorks();
  }

  getTypesOfWorks() {
    this.repository.getData("api/typeofwork").subscribe(res => {
      this.typesofworks = res as TypesOfWorksModel[];
    });
  }

  getSuppliers() {
    this.repository.getData("api/supplier").subscribe(res => {
      this.suppliers = res as SupplierModel[];
    });
  }

  getLocations() {
    this.repository.getData("api/location").subscribe(res => {
      this.locations = res as LocationModel[];
    });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.materialsForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public createMaterials = materialsFormValue => {
    if (this.materialsForm.valid) {
      this.executeMaterialsCreation(materialsFormValue);
    }
  };

  private executeMaterialsCreation = materialsFormValue => {
    let materialForCreate: MaterialModelForCreate = {
      location: materialsFormValue.location,
      typeofmaterial: materialsFormValue.typeofmaterial,
      date: materialsFormValue.date,
      quantity: materialsFormValue.quantity,
      unitofmeasure: materialsFormValue.unitofmeasure,
      typeofwork: materialsFormValue.typeofwork,
      amountPDV: materialsFormValue.amountPDV,
      pricewithPDV: materialsFormValue.pricewithPDV,
      supplier: materialsFormValue.supplier
    };

    this.repository.create("api/material", materialForCreate).subscribe(
      res => {
        let dialogRef = this.dialog.open(
          SuccessDialogComponent,
          this.dialogConfig
        );

        //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
        dialogRef.afterClosed().subscribe(result => {
          this.location.back();
        });
      },
      err => {
        this.location.back();
      }
    );
  };
}
