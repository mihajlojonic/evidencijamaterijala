import { Component, OnInit } from "@angular/core";
import { RepositoryService } from "src/app/shared/repository.service";
import { MatDialog } from "@angular/material";
import { TypesOfWorksModel } from "src/app/interface/TypesOfWorksModel";
import { SupplierModel } from "src/app/interface/SupplierModel";
import { LocationModel } from "src/app/interface/LocationModel";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { MaterialModel } from "src/app/interface/MaterialModel";
import { Location } from "@angular/common";
import { MaterialModelForCreate } from "src/app/interface/ForCreate/MaterialModelForCreate";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-materials-update",
  templateUrl: "./materials-update.component.html",
  styleUrls: ["./materials-update.component.css"]
})
export class MaterialsUpdateComponent implements OnInit {
  public materialsForm: FormGroup;
  public locations: LocationModel[] = [];
  public suppliers: SupplierModel[] = [];
  public typesofworks: TypesOfWorksModel[] = [];
  private dialogConfig;

  public material: MaterialModel = null;

  constructor(
    private location: Location,
    private repository: RepositoryService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getTypesOfWorks();
    this.getSuppliers();
    this.getLocations();

    this.materialsForm = new FormGroup({
      location: new FormControl("", [Validators.required]),
      typeofmaterial: new FormControl("", [
        Validators.required,
        Validators.maxLength(60)
      ]),
      date: new FormControl(new Date()),
      quantity: new FormControl("", [Validators.required]),
      unitofmeasure: new FormControl("", [
        Validators.required,
        Validators.maxLength(100)
      ]),
      typeofwork: new FormControl("", [Validators.required]),
      amountPDV: new FormControl(),
      pricewithPDV: new FormControl(),
      supplier: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };

    this.setMaterialForUpdate();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.materialsForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public setMaterialForUpdate = () => {
    let id: string = this.activeRoute.snapshot.params["id"];
    let apiUrl: string = `api/material/${id}`;
    let materialTmp: MaterialModel;
    this.repository.getData(apiUrl).subscribe(
      res => {
        this.material = res as MaterialModel;

        this.materialsForm.controls["location"].setValue(
          this.material.location._id
        );
        this.materialsForm.controls["date"].setValue(this.material.date);
        this.materialsForm.controls["supplier"].setValue(
          this.material.supplier._id
        );
        this.materialsForm.controls["typeofmaterial"].setValue(
          this.material.typeofmaterial
        );
        this.materialsForm.controls["quantity"].setValue(
          this.material.quantity
        );
        this.materialsForm.controls["unitofmeasure"].setValue(
          this.material.unitofmeasure
        );
        this.materialsForm.controls["typeofwork"].setValue(
          this.material.typeofwork._id
        );
        this.materialsForm.controls["amountPDV"].setValue(
          this.material.amountPDV
        );
        this.materialsForm.controls["pricewithPDV"].setValue(
          this.material.pricewithPDV
        );
      },
      error => {
        console.log(error);
      }
    );
  };

  getTypesOfWorks() {
    this.repository.getData("api/typeofwork").subscribe(res => {
      this.typesofworks = res as TypesOfWorksModel[];
    });
  }

  getSuppliers() {
    this.repository.getData("api/supplier").subscribe(res => {
      this.suppliers = res as SupplierModel[];
    });
  }

  getLocations() {
    this.repository.getData("api/location").subscribe(res => {
      this.locations = res as LocationModel[];
    });
  }

  public updateMaterials = materialsFormValue => {
    if (this.materialsForm.valid) {
      this.executeMaterialsUpdate(materialsFormValue);
    }
  };

  private executeMaterialsUpdate = materialsFormValue => {
    let materialForUpdate: MaterialModelForCreate = {
      location: materialsFormValue.location,
      typeofmaterial: materialsFormValue.typeofmaterial,
      date: materialsFormValue.date,
      quantity: materialsFormValue.quantity,
      unitofmeasure: materialsFormValue.unitofmeasure,
      typeofwork: materialsFormValue.typeofwork,
      amountPDV: materialsFormValue.amountPDV,
      pricewithPDV: materialsFormValue.pricewithPDV,
      supplier: materialsFormValue.supplier
    };

    this.repository
      .update("api/material/" + this.material._id, materialForUpdate)
      .subscribe(
        res => {
          let dialogRef = this.dialog.open(
            SuccessDialogComponent,
            this.dialogConfig
          );

          //we are subscribing on the [mat-dialog-close] attribute as soon as we click on the dialog button
          dialogRef.afterClosed().subscribe(result => {
            this.location.back();
          });
        },
        err => {
          this.location.back();
        }
      );
  };
}
