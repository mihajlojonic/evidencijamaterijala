import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { MaterialModel } from "src/app/interface/MaterialModel";
import { MatTableDataSource, MatDialog, MatPaginator } from "@angular/material";
import { RepositoryService } from "src/app/shared/repository.service";
import { DeleteConfirmComponent } from "src/app/shared/dialogs/delete-confirm/delete-confirm.component";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";
import { Router } from "@angular/router";
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from "pdfmake/build/vfs_fonts";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LocationModel } from "src/app/interface/LocationModel";

@Component({
  selector: "app-materials-list",
  templateUrl: "./materials-list.component.html",
  styleUrls: ["./materials-list.component.css"]
})
export class MaterialsListComponent implements OnInit, AfterViewInit {
  public displayedColumns = [
    "Datum",
    "Dobavljac",
    "Vrsta materijala",
    "Kolicina",
    "Jedinica mere",
    "Vrste radova",
    "Iznos PDV-a",
    "Vrednost sa PDV-om",
    "Izmeni",
    "Obrisi"
  ];

  public searchForm: FormGroup;

  public locations: LocationModel[] = [];

  public dataSource = new MatTableDataSource<MaterialModel>();
  private deleteConfirmDialogConfig;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private repoService: RepositoryService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getLocations();

    this.searchForm = new FormGroup({
      location: new FormControl("", [Validators.required]),
      dateFrom: new FormControl("", [Validators.required]),
      dateTo: new FormControl("", [Validators.required])
    });

    let dateNow = new Date();
    let dateOneMonthBefore = new Date();
    dateOneMonthBefore.setMonth(dateOneMonthBefore.getMonth() - 1);

    this.searchForm.controls["dateFrom"].setValue(dateOneMonthBefore);
    this.searchForm.controls["dateTo"].setValue(dateNow);

    this.deleteConfirmDialogConfig = {
      height: "250px",
      width: "400px",
      disableClose: true,
      data: {}
    };
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  getLocations() {
    this.repoService.getData("api/location").subscribe(res => {
      this.locations = res as LocationModel[];
      this.searchForm.controls["location"].setValue(this.locations[0]._id);
      this.search(this.searchForm.value);
    });
  }

  public getAllMaterials = (
    startDate: string,
    endDate: string,
    location: string
  ) => {
    this.repoService
      .getData(
        "api/material?startDate=" +
          startDate +
          "&endDate=" +
          endDate +
          "&location=" +
          location
      )
      .subscribe(res => {
        this.dataSource.data = res as MaterialModel[];
      });
  };

  public search = searchFormValue => {
    this.getAllMaterials(
      searchFormValue.dateFrom.toISOString(),
      searchFormValue.dateTo.toISOString(),
      searchFormValue.location
    );
  };

  public redirectToUpdate = (id: string) => {
    let url: string = `/materials/update/${id}`;
    this.router.navigate([url]);
  };

  public redirectToDelete = (id: string) => {
    this.deleteConfirmDialogConfig.data = {
      type: "Material",
      valueReturn: false
    };
    let dialogRef = this.dialog.open(
      DeleteConfirmComponent,
      this.deleteConfirmDialogConfig
    );

    dialogRef.afterClosed().subscribe(result => {
      console.log("result after delete confirm dialog closed");
      console.log(this.deleteConfirmDialogConfig.data);
      if (this.deleteConfirmDialogConfig.data.valueReturn == true)
        this.doDelete(id);
    });
  };

  public doDelete = (id: string) => {
    let url: string = `api/material/${id}`;
    this.repoService.delete(url).subscribe(
      res => {
        this.deleteConfirmDialogConfig.data = {};
        let dialogSuccesRef = this.dialog.open(
          SuccessDialogComponent,
          this.deleteConfirmDialogConfig
        );

        dialogSuccesRef.afterClosed().subscribe(result => {
          this.getAllMaterials(
            this.searchForm.controls["dateFrom"].value.toISOString(),
            this.searchForm.controls["dateTo"].value.toISOString(),
            this.searchForm.controls["location"].value
          );
        });
      },
      error => {
        console.log(error);
      }
    );
  };
}
