import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialsListComponent } from "./materials-list/materials-list.component";
import { MaterialsRoutingModule } from "./materials-routing/materials-routing.module";
import { MaterialModule } from "../material/material.module";
import { MaterialsCreateComponent } from "./materials-create/materials-create.component";
import { ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule, MatNativeDateModule } from "@angular/material";
import { SharedModule } from "../shared/shared.module";
import { MaterialsUpdateComponent } from './materials-update/materials-update.component';

@NgModule({
  declarations: [MaterialsListComponent, MaterialsCreateComponent, MaterialsUpdateComponent],
  imports: [
    CommonModule,
    MaterialsRoutingModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule
  ]
})
export class MaterialsModule {}
