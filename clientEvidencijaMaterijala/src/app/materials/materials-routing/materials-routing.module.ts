import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MaterialsListComponent } from "../materials-list/materials-list.component";
import { Routes, RouterModule } from "@angular/router";
import { MaterialsCreateComponent } from "../materials-create/materials-create.component";
import { MaterialsUpdateComponent } from "../materials-update/materials-update.component";

const routes: Routes = [
  { path: "materials", component: MaterialsListComponent },
  { path: "create", component: MaterialsCreateComponent },
  { path: "update/:id", component: MaterialsUpdateComponent }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaterialsRoutingModule {}
