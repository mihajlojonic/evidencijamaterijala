import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import {
  MatTableDataSource,
  MatDialog,
  MatSort,
  MatPaginator
} from "@angular/material";
import { LocationModel } from "src/app/interface/LocationModel";
import { RepositoryService } from "src/app/shared/repository.service";
import { Router } from "@angular/router";
import { DeleteConfirmComponent } from "src/app/shared/dialogs/delete-confirm/delete-confirm.component";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-location-list",
  templateUrl: "./location-list.component.html",
  styleUrls: ["./location-list.component.css"]
})
export class LocationListComponent implements OnInit, AfterViewInit {
  public displayedColumns = ["Lokacija", "Izmeni", "Obrisi"];

  public dataSource = new MatTableDataSource<LocationModel>();
  private deleteConfirmDialogConfig;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private repoService: RepositoryService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getAllLocations();

    this.deleteConfirmDialogConfig = {
      height: "250px",
      width: "400px",
      disableClose: true,
      data: {}
    };
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  public getAllLocations = () => {
    this.repoService.getData("api/location").subscribe(res => {
      this.dataSource.data = res as LocationModel[];
    });
  };

  public redirectToUpdate = (id: string) => {
    let url: string = `/locations/update/${id}`;
    this.router.navigate([url]);
  };

  public redirectToDelete = (id: string) => {
    this.deleteConfirmDialogConfig.data = {
      type: "Location",
      valueReturn: false
    };
    let dialogRef = this.dialog.open(
      DeleteConfirmComponent,
      this.deleteConfirmDialogConfig
    );

    dialogRef.afterClosed().subscribe(result => {
      if (this.deleteConfirmDialogConfig.data.valueReturn == true)
        this.doDelete(id);
    });
  };

  public doDelete = (id: string) => {
    let url: string = `api/location/${id}`;
    this.repoService.delete(url).subscribe(
      res => {
        this.deleteConfirmDialogConfig.data = {};
        let dialogSuccesRef = this.dialog.open(
          SuccessDialogComponent,
          this.deleteConfirmDialogConfig
        );

        dialogSuccesRef.afterClosed().subscribe(result => {
          this.getAllLocations();
        });
      },
      error => {
        console.log(error);
      }
    );
  };
}
