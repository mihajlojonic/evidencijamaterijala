import { Component, OnInit } from "@angular/core";
import { LocationModel } from "src/app/interface/LocationModel";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RepositoryService } from "src/app/shared/repository.service";
import { MatDialog } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { LocationModelForCreate } from "src/app/interface/ForCreate/LocationModelForCreate";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-location-update",
  templateUrl: "./location-update.component.html",
  styleUrls: ["./location-update.component.css"]
})
export class LocationUpdateComponent implements OnInit {
  public locationsForm: FormGroup;
  private dialogConfig;

  public locationModel: LocationModel = null;

  constructor(
    private location: Location,
    private repository: RepositoryService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.locationsForm = new FormGroup({
      location: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };

    this.setLocationForUpdate();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.locationsForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public setLocationForUpdate = () => {
    let id: string = this.activeRoute.snapshot.params["id"];
    let apiUrl: string = `api/location/${id}`;
    let materialTmp: LocationModel;
    this.repository.getData(apiUrl).subscribe(
      res => {
        this.locationModel = res as LocationModel;

        this.locationsForm.controls["location"].setValue(
          this.locationModel.name
        );
      },
      error => {
        console.log(error);
      }
    );
  };

  public updateLocations = locationsFormValue => {
    if (this.locationsForm.valid) {
      this.executeLocationsUpdate(locationsFormValue);
    }
  };

  private executeLocationsUpdate = locationsFormValue => {
    let locationForUpdate: LocationModelForCreate = {
      name: locationsFormValue.location
    };

    this.repository
      .update("api/location/" + this.locationModel._id, locationForUpdate)
      .subscribe(
        res => {
          let dialogRef = this.dialog.open(
            SuccessDialogComponent,
            this.dialogConfig
          );

          dialogRef.afterClosed().subscribe(result => {
            this.location.back();
          });
        },
        err => {
          this.location.back();
        }
      );
  };
}
