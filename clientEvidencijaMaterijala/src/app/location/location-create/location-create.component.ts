import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RepositoryService } from "src/app/shared/repository.service";
import { MatDialog } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { LocationModelForCreate } from "src/app/interface/ForCreate/LocationModelForCreate";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-location-create",
  templateUrl: "./location-create.component.html",
  styleUrls: ["./location-create.component.css"]
})
export class LocationCreateComponent implements OnInit {
  public locationsForm: FormGroup;
  private dialogConfig;

  constructor(
    private location: Location,
    private repository: RepositoryService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.locationsForm = new FormGroup({
      locationName: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.locationsForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public createLocation = locationFormValue => {
    if (this.locationsForm.valid) {
      this.executeLocationCreation(locationFormValue);
    }
  };

  private executeLocationCreation = locationFormValue => {
    let locationForCreate: LocationModelForCreate = {
      name: locationFormValue.locationName
    };

    this.repository.create("api/location", locationForCreate).subscribe(
      res => {
        let dialogRef = this.dialog.open(
          SuccessDialogComponent,
          this.dialogConfig
        );

        dialogRef.afterClosed().subscribe(result => {
          this.location.back();
        });
      },
      err => {
        this.location.back();
      }
    );
  };
}
