import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LocationListComponent } from "./location-list/location-list.component";
import { LocationCreateComponent } from "./location-create/location-create.component";
import { LocationUpdateComponent } from "./location-update/location-update.component";
import { LocationRoutingModule } from "./location-routing/location-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [
    LocationListComponent,
    LocationCreateComponent,
    LocationUpdateComponent
  ],
  imports: [
    CommonModule,
    LocationRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class LocationModule {}
