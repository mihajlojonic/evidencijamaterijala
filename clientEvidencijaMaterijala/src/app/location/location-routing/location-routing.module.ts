import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { LocationListComponent } from "../location-list/location-list.component";
import { LocationCreateComponent } from "../location-create/location-create.component";
import { LocationUpdateComponent } from "../location-update/location-update.component";

const routes: Routes = [
  { path: "locations", component: LocationListComponent },
  { path: "create", component: LocationCreateComponent },
  { path: "update/:id", component: LocationUpdateComponent }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationRoutingModule {}
