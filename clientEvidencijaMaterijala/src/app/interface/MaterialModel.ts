import { LocationModel } from "./LocationModel";
import { TypesOfWorksModel } from "./TypesOfWorksModel";
import { SupplierModel } from "./SupplierModel";

export interface MaterialModel {
  _id: String;
  location: LocationModel;
  date: Date;
  supplier: SupplierModel;
  typeofmaterial: String;
  quantity: Number;
  unitofmeasure: String;
  typeofwork: TypesOfWorksModel;
  amountPDV: Number;
  pricewithPDV: Number;
}
