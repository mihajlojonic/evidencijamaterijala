export interface MaterialModelForCreate {
  location: String;
  date: Date;
  supplier: String;
  typeofmaterial: String;
  quantity: Number;
  unitofmeasure: String;
  typeofwork: String;
  amountPDV: Number;
  pricewithPDV: Number;
}
