import { MaterialModel } from "../MaterialModel";

export interface FullReportModel {
  materials: MaterialModel[];
  sumPDV: number;
  sumPriceWithPDV: number;
}
