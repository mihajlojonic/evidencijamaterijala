import { FullReportModel } from "./FullReportModel";
import { GroupedReportItem } from "./GroupedReportItem";

export interface ReportModel {
  fullReport: FullReportModel;
  groupedBySuppliers: GroupedReportItem[];
}
