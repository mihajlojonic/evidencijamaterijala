import { SupplierModel } from "../SupplierModel";
import { MaterialModel } from "../MaterialModel";

export interface GroupedReportItem {
  supplier: SupplierModel;
  materials: MaterialModel[];
  sumPDV: number;
  sumPriceWithPDV: number;
}
