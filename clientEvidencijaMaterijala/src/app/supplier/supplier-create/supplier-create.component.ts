import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RepositoryService } from "src/app/shared/repository.service";
import { MatDialog } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { SupplierModelForCreate } from "src/app/interface/ForCreate/SupplierModelForCreate";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-supplier-create",
  templateUrl: "./supplier-create.component.html",
  styleUrls: ["./supplier-create.component.css"]
})
export class SupplierCreateComponent implements OnInit {
  public suppliersForm: FormGroup;
  private dialogConfig;

  constructor(
    private location: Location,
    private repository: RepositoryService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.suppliersForm = new FormGroup({
      supplierName: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.suppliersForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public createSupplier = supplierFormValue => {
    if (this.suppliersForm.valid) {
      this.executeSupplierCreation(supplierFormValue);
    }
  };

  private executeSupplierCreation = supplierFormValue => {
    let supplierForCreate: SupplierModelForCreate = {
      supplierName: supplierFormValue.supplierName
    };

    this.repository.create("api/supplier", supplierForCreate).subscribe(
      res => {
        let dialogRef = this.dialog.open(
          SuccessDialogComponent,
          this.dialogConfig
        );

        dialogRef.afterClosed().subscribe(result => {
          this.location.back();
        });
      },
      err => {
        this.location.back();
      }
    );
  };
}
