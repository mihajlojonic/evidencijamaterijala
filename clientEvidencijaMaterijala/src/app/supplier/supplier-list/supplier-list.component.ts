import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { SupplierModel } from "src/app/interface/SupplierModel";
import {
  MatTableDataSource,
  MatDialog,
  MatSort,
  MatPaginator
} from "@angular/material";
import { RepositoryService } from "src/app/shared/repository.service";
import { Router } from "@angular/router";
import { DeleteConfirmComponent } from "src/app/shared/dialogs/delete-confirm/delete-confirm.component";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-supplier-list",
  templateUrl: "./supplier-list.component.html",
  styleUrls: ["./supplier-list.component.css"]
})
export class SupplierListComponent implements OnInit, AfterViewInit {
  public displayedColumns = ["Dobavljac", "Izmeni", "Obrisi"];

  public dataSource = new MatTableDataSource<SupplierModel>();
  private deleteConfirmDialogConfig;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private repoService: RepositoryService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getAllSuppliers();

    this.deleteConfirmDialogConfig = {
      height: "250px",
      width: "400px",
      disableClose: true,
      data: {}
    };
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  public getAllSuppliers = () => {
    this.repoService.getData("api/supplier").subscribe(res => {
      this.dataSource.data = res as SupplierModel[];
    });
  };

  public redirectToUpdate = (id: string) => {
    let url: string = `/suppliers/update/${id}`;
    this.router.navigate([url]);
  };

  public redirectToDelete = (id: string) => {
    this.deleteConfirmDialogConfig.data = {
      type: "Supplier",
      valueReturn: false
    };
    let dialogRef = this.dialog.open(
      DeleteConfirmComponent,
      this.deleteConfirmDialogConfig
    );

    dialogRef.afterClosed().subscribe(result => {
      if (this.deleteConfirmDialogConfig.data.valueReturn == true)
        this.doDelete(id);
    });
  };

  public doDelete = (id: string) => {
    let url: string = `api/supplier/${id}`;
    this.repoService.delete(url).subscribe(
      res => {
        this.deleteConfirmDialogConfig.data = {};
        let dialogSuccesRef = this.dialog.open(
          SuccessDialogComponent,
          this.deleteConfirmDialogConfig
        );

        dialogSuccesRef.afterClosed().subscribe(result => {
          this.getAllSuppliers();
        });
      },
      error => {
        console.log(error);
      }
    );
  };
}
