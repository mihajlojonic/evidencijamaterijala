import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SupplierListComponent } from "./supplier-list/supplier-list.component";
import { SupplierCreateComponent } from "./supplier-create/supplier-create.component";
import { SupplierUpdateComponent } from "./supplier-update/supplier-update.component";
import { SupplierRoutingModule } from "./supplier-routing/supplier-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [
    SupplierListComponent,
    SupplierCreateComponent,
    SupplierUpdateComponent
  ],
  imports: [
    CommonModule,
    SupplierRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class SupplierModule {}
