import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { SupplierListComponent } from "../supplier-list/supplier-list.component";
import { SupplierCreateComponent } from "../supplier-create/supplier-create.component";
import { SupplierUpdateComponent } from "../supplier-update/supplier-update.component";

const routes: Routes = [
  { path: "suppliers", component: SupplierListComponent },
  { path: "create", component: SupplierCreateComponent },
  { path: "update/:id", component: SupplierUpdateComponent }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplierRoutingModule {}
