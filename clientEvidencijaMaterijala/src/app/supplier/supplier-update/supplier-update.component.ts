import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { SupplierModel } from "src/app/interface/SupplierModel";
import { Location } from "@angular/common";
import { RepositoryService } from "src/app/shared/repository.service";
import { MatDialog } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { SupplierModelForCreate } from "src/app/interface/ForCreate/SupplierModelForCreate";
import { SuccessDialogComponent } from "src/app/shared/dialogs/success-dialog/success-dialog.component";

@Component({
  selector: "app-supplier-update",
  templateUrl: "./supplier-update.component.html",
  styleUrls: ["./supplier-update.component.css"]
})
export class SupplierUpdateComponent implements OnInit {
  public suppliersForm: FormGroup;
  private dialogConfig;

  public supplierModel: SupplierModel = null;

  constructor(
    private location: Location,
    private repository: RepositoryService,
    private dialog: MatDialog,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.suppliersForm = new FormGroup({
      supplierName: new FormControl("", [Validators.required])
    });

    this.dialogConfig = {
      height: "200px",
      width: "400px",
      disableClose: true,
      data: {}
    };

    this.setSupplierForUpdate();
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.suppliersForm.controls[controlName].hasError(errorName);
  };

  public onCancel = () => {
    this.location.back();
  };

  public setSupplierForUpdate = () => {
    let id: string = this.activeRoute.snapshot.params["id"];
    let apiUrl: string = `api/supplier/${id}`;
    let supplierTmp: SupplierModel;
    this.repository.getData(apiUrl).subscribe(
      res => {
        this.supplierModel = res as SupplierModel;

        this.suppliersForm.controls["supplierName"].setValue(
          this.supplierModel.supplierName
        );
      },
      error => {
        console.log(error);
      }
    );
  };

  public updateSuppliers = suppliersFormValue => {
    if (this.suppliersForm.valid) {
      this.executeSuppliersUpdate(suppliersFormValue);
    }
  };

  private executeSuppliersUpdate = suppliersFormValue => {
    let supplierForUpdate: SupplierModelForCreate = {
      supplierName: suppliersFormValue.supplierName
    };

    this.repository
      .update("api/supplier/" + this.supplierModel._id, supplierForUpdate)
      .subscribe(
        res => {
          let dialogRef = this.dialog.open(
            SuccessDialogComponent,
            this.dialogConfig
          );

          dialogRef.afterClosed().subscribe(result => {
            this.location.back();
          });
        },
        err => {
          this.location.back();
        }
      );
  };
}
